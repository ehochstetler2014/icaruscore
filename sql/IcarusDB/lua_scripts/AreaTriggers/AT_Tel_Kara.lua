-- Update plr:Teleport() Func with Enterance Coords
-- Two More Triggers need made for Kara
-- One for Wizards Encounter
-- One for Final Boss
local NPC_ID = 129990 -- Invisible Word Trigger (Kara Test Tel)
local Teleport_Range = 1 -- Range in yards
     
function AreaTrigger_OnSpawn(event, creature)
        creature:SetVisible(false)
        creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Teleport_Range)) then
                plr:Teleport(532, -11091.2, -1992.3, 49.8, 0.9) -- Karazhan Start
        end
end
  
RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)