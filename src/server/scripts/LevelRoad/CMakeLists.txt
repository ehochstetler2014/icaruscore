# Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(scripts_STAT_SRCS
  ${scripts_STAT_SRCS}
  LevelRoad/MobAI/Levelroad-Zombies.cpp
  LevelRoad/MobAI/RandomMover.cpp
  LevelRoad/npc_player_helper.cpp
  LevelRoad/npc_player_helper.h
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossOne.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossTwo.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossThree.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossFour.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossFive.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossSix.cpp
  LevelRoad/HyjalMiniBosses/1-10/Levelroad-MinibossSeven.cpp
  LevelRoad/QuestNPCs/npc_soulThief_spy.cpp
)

message("  -> Prepared: Hyjal LevelRoad")
