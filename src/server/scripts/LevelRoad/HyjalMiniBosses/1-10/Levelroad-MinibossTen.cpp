/*
**********************************
*      Icarus-Gaming Inc.        *
*  MiniBossSeven Level Road IA   *
*    Type: LevelRoad MiniBoss    *
*      By Matthew Ferrill        *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 100 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells // Remake Spells -> Spell Lookup @ http://openwow.com
{

};

class npc_minibossten : public CreatureScript
{
public:
	npc_minibossten() : CreatureScript("npc_minibossten") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibosstenAI (creature);
	}

	struct npc_minibosstenAI : public ScriptedAI
	{
		npc_minibosstenAI(Creature* creature) : ScriptedAI(creature) {}
		
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			// Add New Spell Timers
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			
			/*DarkFuryTimer = urand(45000, 60000);
			FlashOfDarknessTimer = 25000;
			DarkOfNightTimer = 20000;
			DoomFireTimer = 40000;
			CurseOfAgonyTimer = 35000;*/ 
			MiniBossInCombat = false;
			// Add new VI ID no
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 40497); // Black Ice PoleArm
		}
		// Add new txt mssgs for creature emotes
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("I'll bring you to the Soul Thief later for a nice reward!!!", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("...there you are, Death... my old friend...", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Not the best of life decisions little mortals!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("Good Lord!  All this standing about really wears on a guy!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000);
					} else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("All of this land will perish under the Soul Thief's rule!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); 
					} else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("Death shall come to all that cross my path!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = urand(25000, 80000); 
					} else TalkTimerThree -= diff;
			}						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (DarkFuryTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_DARK_FURY, true);
					me->MonsterYell("My powers cannot be contained. They move and writhe from one victim to another!!!", LANG_UNIVERSAL, me);
					DarkFuryTimer = urand(30000, 45000);
					}
					else DarkFuryTimer -= diff;
				
				if (FlashOfDarknessTimer <= diff)
					{
					DoCast(me->GetVictim(), SPELL_FLASH_OF_DARKNESS, true);
					me->MonsterYell("All those within my gaze will suffer!!!", LANG_UNIVERSAL, me);
					FlashOfDarknessTimer = urand(50000, 65000);
					}
					else FlashOfDarknessTimer -= diff;
					
				if (DarkOfNightTimer <= diff)
					{
					DoCast(me, SPELL_DARK_OF_NIGHT, true);
					me->MonsterYell("My Powers Grow Even Stronger!!!", LANG_UNIVERSAL, me);
					DarkOfNightTimer = urand(30000, 45000);
					}
					else DarkOfNightTimer -= diff;
									
				if (DoomFireTimer <= diff)
					{
					DoCast(me, SPELL_DOOM_FIRE, true);
					me->MonsterYell("Flee in terror from my powers foolish mortals!!!", LANG_UNIVERSAL, me);
					DoomFireTimer = urand(45000, 60000);
					}
					else DoomFireTimer -= diff;
					
				if (CurseOfAgonyTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0)) 
						{
						DoCast(Target, SPELL_CURSE_OF_AGONY, true);
						me->MonsterYell("A curse upon your soul, foolish 'Hero'!!!", LANG_UNIVERSAL, me);
						CurseOfAgonyTimer = urand(25000, 70000);
						}
					}else CurseOfAgonyTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}			
	
	};
};

void AddSC_npc_minibossten()
{
	new npc_minibossten();
}		