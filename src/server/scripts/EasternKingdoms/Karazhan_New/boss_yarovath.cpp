/*
*********************************
*		BotCore Custom Boss 	*
*		Yarovath 				*
*		Type: Boss Encounter 	*
*		By Josh Carter 			*
*********************************
*/

/* ScriptData
SDCategory: Karazhan
SDName: Boss_Yarovath
SD%Complete: 100% Needs in game testing
SDComment:
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_yarovath.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

class boss_yarovath : public CreatureScript
{
public:
	boss_yarovath() : CreatureScript("boss_yarovath") { }

	bool OnGossipHello(Player * player, Creature * creature)
	{
		if(player->IsInCombat())
			return false;

		if(y_isBattleActive)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "You will all die horrible deaths!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
		else
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, MSG_FIGHT_COMPUTER, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Run away and live", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player * player, Creature * creature, uint32 sender, uint32 actions)
	{
		if(sender != GOSSIP_SENDER_MAIN)
			return false;
		player->PlayerTalkClass->ClearMenus();
		switch(actions)
		{
		case GOSSIP_ACTION_INFO_DEF+1:

			y_PlayerGUID = player->GetGUID();
			y_playerName = player->GetName();
			y_isBattleActive = true;
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF+2:
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF+3:
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}
		return true;
	}

	struct boss_yarovathAI : public ScriptedAI
	{
		boss_yarovathAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 checkBattle;
		uint32 checkPlayer;
		bool checkIsDead;
		bool resetOnce;

		void Reset()
		{
			events.Reset();
			player = NULL;
			summons.DespawnAll();
			checkIsDead = true;
			resetOnce = false;
			checkBattle = 2000;
			checkPlayer = 1000;
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			events.Update(diff);

			if(checkBattle <= diff)
			{
				if(!y_isBattleActive && y_PlayerGUID == 0 && !resetOnce)
				{
					events.Reset();
					resetOnce = true;
				}

				if(y_isBattleActive && y_PlayerGUID != 0 && resetOnce)
				{
					player = Unit::GetPlayer(*me, y_PlayerGUID);
					events.ScheduleEvent(EVENT_CHECK_ACTIVITY, 1000);
					resetOnce = false;
				}
				if(resetOnce)
					checkBattle = 2000;
			}
			else
				checkBattle -= diff;

			if(checkPlayer <= diff) // Checking battle as well
			{
				if(y_PlayerGUID == 0)
					return;

				if(y_hasLogged || !y_inZone)
				{
					y_isBattleActive = false;
					summons.DespawnAll();
					events.Reset();
					y_isWaveBossDead = 0;
					checkIsDead = true;
					y_hasLogged = false;
					y_inZone = true;
					resetOnce = false;
					player = NULL;
					y_PlayerGUID = NULL;
					y_playerName = "";
					sWorld->SendGlobalText("decides to leave... A wise decision", NULL);
				}
				checkPlayer = 1000;
			}
			else
				checkPlayer -= diff;

			if (y_isWaveBossDead == 1 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}
			else if (y_isWaveBossDead == 2 && !checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = true;
			}
			else if (y_isWaveBossDead == 3 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}
			else if (y_isWaveBossDead == 4 && !checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = true;
			}

			while(uint32 eventIds = events.ExecuteEvent())
			{
				switch(eventIds)
				{
				case EVENT_CHECK_ACTIVITY:
					{
						if(y_isBattleActive)
						{
							yMessageOnWave(me, EVENT_CHECK_ACTIVITY);
							events.ScheduleEvent(EVENT_FIRST_WAVE, 10000);
						}
						else
							events.ScheduleEvent(EVENT_CHECK_ACTIVITY, 1000);
					}break;

				case EVENT_CHECK_WAVES:
					{
						if(!player)
							return;

						if(!y_isBattleActive)
						{
							summons.DespawnAll();
							checkIsDead = true;
							resetOnce = false;
							return;
						}

						if(y_isWaveBossDead == 1) // 1st Mini Boss
						{
							yMessageOnWave(me, EVENT_CHECK_WAVES);
							events.ScheduleEvent(EVENT_SECOND_WAVE, 25000);
							y_isWaveBossDead = 0; // This should be first wave Trash
						}

						if (y_isWaveBossDead == 2) // 2nd Mini Boss
						{
							yMessageOnWave(me, EVENT_CHECK_WAVES);
							events.ScheduleEvent(EVENT_THIRD_WAVE, 25000);
							y_isWaveBossDead = 0;
						}

						if(y_isWaveBossDead == 3) // 3nd Mini Boss
						{
							yMessageOnWave(me, EVENT_CHECK_WAVES);
							events.ScheduleEvent(EVENT_FOURTH_WAVE, 35000);
							y_isWaveBossDead = 0;
						}

						if(y_isWaveBossDead == 4) // 4th Mini Boss
						{
							yMessageOnWave(me, EVENT_CHECK_WAVES);
							events.ScheduleEvent(EVENT_COMPLETED_WAVES, 35000);
							y_isWaveBossDead = 0;
						}
					}break;

				case EVENT_FIRST_WAVE:
					TC_LOG_INFO("misc", "[Disembodied Voice]: Prepare Yourselves...");
					yMessageOnWave(me, EVENT_FIRST_WAVE);
					me->SummonCreature(y_waveList[0], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(y_waveList[1], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_SECOND_WAVE:
					yMessageOnWave(me, EVENT_SECOND_WAVE);
					me->SummonCreature(y_waveList[2], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(y_waveList[3], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_THIRD_WAVE:
					yMessageOnWave(me, EVENT_THIRD_WAVE);
					me->SummonCreature(y_waveList[4], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(y_waveList[5], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_FOURTH_WAVE:
					yMessageOnWave(me, EVENT_FOURTH_WAVE);
					me->SummonCreature(y_waveList[6], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(y_waveList[7], y_WaveSpawns[0].m_positionX, y_WaveSpawns[0].m_positionY, y_WaveSpawns[0].m_positionZ, y_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_COMPLETED_WAVES:
					me->MonsterYell("You have proven worthy of life, but I am busy, go now.",
						LANG_UNIVERSAL, me);
					player->TeleportTo(532, sTeleOut[0].m_positionX, sTeleOut[0].m_positionY, sTeleOut[0].m_positionZ, sTeleOut[0].m_orientation);
					y_isBattleActive = false;
					y_PlayerGUID = NULL;
					y_playerName = "";
					summons.DespawnAll();
					events.Reset();
					y_isWaveBossDead = 0;
					checkIsDead = true;
					break;
				}
			}
		}

		void JustSummoned(Creature * summoned)
		{
			summons.Summon(summoned);
		}
	private:
		EventMap events;
		Player * player;
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new boss_yarovathAI(pCreature);
	}
};

class npc_y_mini_boss_portal : public CreatureScript
{
public:
	npc_y_mini_boss_portal() : CreatureScript("npc_y_mini_boss_portal") { }

	struct npc_y_mini_boss_portalAI : public ScriptedAI
	{
		npc_y_mini_boss_portalAI(Creature * c) : ScriptedAI(c) { }

		void Reset()
		{
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!y_isBattleActive)
				me->DespawnOrUnsummon(1);

			if(y_isWaveBossDead == 2)
				me->DespawnOrUnsummon(1);
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_y_mini_boss_portalAI(pCreature);
	}
};

class npc_y_mini_boss_one : public CreatureScript
{
public:
	npc_y_mini_boss_one() : CreatureScript("npc_y_mini_boss_one") { }

	struct npc_y_mini_boss_oneAI : public ScriptedAI
	{
		npc_y_mini_boss_oneAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiBoneSliceTimer;
		uint32 uiEnrageTimer;
		uint32 uiDeathandDecayTimer;
		uint32 uiDeathCoilTimer;
		bool boneArmor;
		bool antiMagic;
		bool shadowStorm;
					
		void Reset()
		{
			uiBoneSliceTimer		= 30000;  // 30 seconds.
			uiEnrageTimer			= 600000; // 10 minutes
			uiDeathandDecayTimer	= 45000; // 45 seconds
			uiDeathCoilTimer		= urand(25000, 50000);
			boneArmor = true;
			antiMagic = true;
			shadowStorm = true;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51516); // Wrathful Gladiator's Hand Axe
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51516); // Wrathful Gladiator's Hand Axe
		}

		void JustDied(Unit* /*killer*/) 
		{
			Talk(SAY_NOT_OVER_1);
			y_isWaveBossDead = 1;
		}
		void OnCombatStart(Unit * /* who */) 
		{
			Talk(SAY_AGGRO_1);
		}

		void KilledUnit(Unit* /*Victim*/) 
		{
			Talk(SAY_PATHETIC_1);
			//yDoEndBattle(me);
		}

		void UpdateAI(uint32 diff) 
		{
			if(!UpdateVictim())
				return;

			ScriptedAI::UpdateAI(diff);
			if(!y_isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}
			/* Enrage Function */
			if (uiEnrageTimer <=diff && !uiEnrageTimer)
			{
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
			}
			else
				uiEnrageTimer -= diff;

			/* Non-repeating stuff */
			if(HealthBelowPct(35) && boneArmor)
			{
				me->AddAura(SPELL_BONE_SHIELD, me);
				boneArmor = false;
			}
			if(HealthBelowPct(25) && antiMagic)
			{
				me->AddAura(SPELL_ANTIMAGIC_SHELL, me);
				antiMagic = false;
			}
			if(HealthBelowPct(100) && shadowStorm) // Enable Shadowstorm at all times
			{
				me->AddAura(SPELL_SHADOW_STORM, me);
				shadowStorm = false;
			}
			/* Repeating stuff */
			
			if (uiDeathandDecayTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_DEATH_AND_DECAY, true); 
					me->MonsterYell("Death and Decay surround you!!!", LANG_UNIVERSAL, me);
					uiDeathandDecayTimer = urand(20000, 40000); 
				}
			} else uiDeathandDecayTimer -= diff;
			
			if (uiDeathCoilTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_DEATHCOIL); 
					me->MonsterYell("Feel the coil of my deathly embrace!!!", LANG_UNIVERSAL, me);
					uiDeathCoilTimer = urand(25000, 55000); 
				}
			} else uiDeathCoilTimer -= diff;
			
			if(uiBoneSliceTimer <= diff)
			{
				DoCastVictim(SPELL_BONE_SLICE, true);
				me->MonsterYell("I will slice your bones in two!!!", LANG_UNIVERSAL, me);
				uiBoneSliceTimer = 50000;
			}
			else
				uiBoneSliceTimer -= diff;

			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_y_mini_boss_oneAI(pCreature);
	}
};

class npc_y_mini_boss_two : public CreatureScript
{
public:
	npc_y_mini_boss_two() : CreatureScript("npc_y_mini_boss_two") { }

	struct npc_y_mini_boss_twoAI : public ScriptedAI
	{
		npc_y_mini_boss_twoAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiDeathBloomTimer;
		uint32 uiBloodPlagueTimer;
		uint32 uiBroodPlagueTimer;
		uint32 uiEnrageTimer;

		void Reset()
		{
			uiDeathBloomTimer    	 = urand(45000, 65000);
			uiBloodPlagueTimer		 = urand(48000, 72000);
			uiBroodPlagueTimer		 = urand(25000, 38000);
			uiEnrageTimer			 = 600000; // 10 minutes
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 36556); // Apocalyptic Staff
		}

		void JustDied(Unit* /*killer*/) 
		{
			Talk(SAY_NOT_OVER_2);
			y_isWaveBossDead = 2;
		}
		void OnCombatStart(Unit * /* who */) 
		{
			Talk(SAY_AGGRO_2);
		}

		void KilledUnit(Unit* /*Victim*/) 
		{
			Talk(SAY_PATHETIC_2);
			//yDoEndBattle(me);
		}

		void UpdateAI(uint32 diff) 
		{
			if(!UpdateVictim())
				return;

			ScriptedAI::UpdateAI(diff);
			if(!y_isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}

			/* Enrage Function */
			if (uiEnrageTimer <=diff && !uiEnrageTimer)
			{
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
			}
			else
				uiEnrageTimer -= diff;

			if (uiDeathBloomTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_DEATH_BLOOM);
					DoCast(Target, SPELL_BLACK_PLAGUE, true); // Failed Testing -> Maybe Channeling? Added true to re-test
					me->MonsterYell("I curse you!!!", LANG_UNIVERSAL, me);
					uiDeathBloomTimer = urand(20000, 50000); // lowered timer for test
				}
			} else uiDeathBloomTimer -= diff;

			if(uiBloodPlagueTimer <= diff)
			{
				me->MonsterYell("Feel my plague enter your very blood!!!", LANG_UNIVERSAL, me);
				DoCastVictim(SPELL_BLOOD_PLAGUE, true);
				uiBloodPlagueTimer = urand(48000, 72000);
			}
			else
				uiBloodPlagueTimer -= diff;

			if(uiBroodPlagueTimer <= diff)
			{
				me->MonsterYell("My plague infests you, HAHAHA!!!", LANG_UNIVERSAL, me);
				DoCastVictim(SPELL_BROOD_PLAGUE, true);
				uiBroodPlagueTimer = 30000;
			}
			else
				uiBroodPlagueTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_y_mini_boss_twoAI(pCreature);
	}
};

class npc_y_mini_boss_three : public CreatureScript
{
public:
	npc_y_mini_boss_three() : CreatureScript("npc_y_mini_boss_three") { }

	struct npc_y_mini_boss_threeAI : public ScriptedAI
	{
		npc_y_mini_boss_threeAI(Creature * c) : ScriptedAI(c) { }

		uint32 FrostBreathTimer;
		uint32 ChainsOfIceTimer;
		uint32 IceBoltTimer;
		uint32 uiEnrageTimer;

		void Reset()
		{
			FrostBreathTimer		= 30000; // 30 seconds.
			ChainsOfIceTimer		= 60000; // 1 minute.
			IceBoltTimer			= urand(15000, 45000); // 15 to 45 seconds.
			uiEnrageTimer			= 600000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 45457); // Staff of Endless Winter
		}

		void JustDied(Unit* /*killer*/) 
		{
			Talk(SAY_NOT_OVER_3);
			y_isWaveBossDead = 3;
		}
		void OnCombatStart(Unit * /* who */) 
		{
			Talk(SAY_AGGRO_3);
		}

		void KilledUnit(Unit* /*Victim*/) 
		{
			Talk(SAY_PATHETIC_3);
			//yDoEndBattle(me);
		}

		void UpdateAI(uint32 diff) 
		{
			if(!UpdateVictim())
				return;

			ScriptedAI::UpdateAI(diff);
			if(!y_isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}

			/* Enrage Function */
			if (uiEnrageTimer <=diff && !uiEnrageTimer)
			{
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
			}
			else
				uiEnrageTimer -= diff;

			if(FrostBreathTimer <= diff)
			{
				DoCast(me, SPELL_INCREASE_FROST_DAMAGE);
				DoCast(me->GetVictim(), SPELL_FROST_BREATH);
				me->MonsterYell("Feel my cold kiss of death!!!", LANG_UNIVERSAL, me);
				FrostBreathTimer = urand(30000, 50000);
			}
			else
				FrostBreathTimer -= diff;

			if(ChainsOfIceTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_CHAINS_OF_ICE);
					me->MonsterYell("You I'll bind with chains of ice!!!", LANG_UNIVERSAL, me);
					ChainsOfIceTimer = urand(30000, 75000);
				}
			}
			else
				ChainsOfIceTimer -= diff;

			if(IceBoltTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_BOTTOMAGGRO, 0)) // Test -> weakest gets hit.
				{
					DoCast(Target, SPELL_ICE_BOLT);
					DoCast(Target, SPELL_ICE_LANCE);
					DoCast(Target, SPELL_ICE_BLAST);
					me->MonsterYell("Freeze you pathetic weakling!!!", LANG_UNIVERSAL, me);
					IceBoltTimer = urand(15000, 45000);
				}
			}
			else
				IceBoltTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_y_mini_boss_threeAI(pCreature);
	}
};

class npc_y_mini_boss_four : public CreatureScript
{
public:
	npc_y_mini_boss_four() : CreatureScript("npc_y_mini_boss_four") { }

	struct npc_y_mini_boss_fourAI : public ScriptedAI
	{
		npc_y_mini_boss_fourAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiFlamestrikeTimer;
		uint32 uiMoltenSpewTimer;
		uint32 uiRainOfFireTimer;
		uint32 uiEnrageTimer;
		uint32 uiLavaBurstTimer;

		void Reset()
		{
			uiLavaBurstTimer			= urand(10000, 58000);
			uiFlamestrikeTimer      	= urand(15000, 60000);
			uiMoltenSpewTimer			= urand(35000, 50000);
			uiRainOfFireTimer			= urand(25000, 40000);
			uiEnrageTimer				= 600000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 48523); // Relentless Gladiator's Greatstaff
		}

		void JustDied(Unit* /*killer*/) 
		{
			Talk(SAY_NOT_OVER_4);
			y_isWaveBossDead = 4;
		}

		void OnCombatStart(Unit * /* who */) 
		{
			Talk(SAY_AGGRO_4);
		}

		void KilledUnit(Unit* /*Victim*/) 
		{
			Talk(SAY_PATHETIC_4);
			//yDoEndBattle(me);
		}

		void UpdateAI(uint32 diff) 
		{
			if(!UpdateVictim())
				return;

			ScriptedAI::UpdateAI(diff);
			if(!y_isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}
			/* Enrage Function */
			if (uiEnrageTimer <=diff && !uiEnrageTimer)
			{
				DoCast(me, SPELL_FRENZY, true);
				DoCast(me, SPELL_MASTER_BUFF_MELEE, true);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC, true);
				DoCast(me, SPELL_FURY_OF_FROSTMOUNE, true);
			}
			else
				uiEnrageTimer -= diff;

				if (uiLavaBurstTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_FLAME_SHOCK);
					DoCast(Target, SPELL_LAVA_BURST, true);
					me->MonsterYell("I'll burn a hole through your heart little hero!!!", LANG_UNIVERSAL, me);
					uiLavaBurstTimer = urand(22000, 55000);
				}
			}
			else
				uiLavaBurstTimer -= diff;
				
			if (uiFlamestrikeTimer <= diff)
			{
				if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
				{
					DoCast(Target, SPELL_MASSIVE_FIREBALL);
					DoCast(Target, SPELL_CHARRED_EARTH);
					me->MonsterYell("FlameStrike!!!", LANG_UNIVERSAL, me);
					uiFlamestrikeTimer = urand(25000, 85000);
				}
			}
			else
				uiFlamestrikeTimer -= diff;

			if (uiMoltenSpewTimer <= diff)
			{
				DoCastVictim(SPELL_DRAGON_BREATH, true);
				DoCastVictim(SPELL_CHARRED_EARTH, true);
				me->MonsterYell("Unworthy Fool!!! I spit on you!!!", LANG_UNIVERSAL, me);
				uiMoltenSpewTimer = urand(45000, 65000);
			}
			else
				uiMoltenSpewTimer -= diff;

			if (uiRainOfFireTimer <= diff)
			{
				me->MonsterYell("Let the fires of Hell rain down upon you!!!", LANG_UNIVERSAL, me);
				DoCastVictim(SPELL_CHARRED_EARTH, true);
				DoCastVictim(SPELL_RAIN_OF_FIRE, true);
				uiRainOfFireTimer = urand(10000, 65000);
			}
			else
				uiRainOfFireTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_y_mini_boss_fourAI(pCreature);
	}
};

void OnLogout(Player * player) 
{
	if(y_PlayerGUID == 0)
		return;

	if(player->GetGUID() == y_PlayerGUID)
		y_hasLogged = true;
}

void AddSC_yarovath_battle()
{
	/* Npc Classes */
	new boss_yarovath;
	new npc_y_mini_boss_portal;
	new npc_y_mini_boss_one;
	new npc_y_mini_boss_two;
	new npc_y_mini_boss_three;
	new npc_y_mini_boss_four;
}