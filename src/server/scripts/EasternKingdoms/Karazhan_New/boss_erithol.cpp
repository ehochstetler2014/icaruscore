/*
* Copyright (C) 2012-2013 Umbra Gaming Inc <http://www.trinitycore.org/>
* Copyright (C) 2012-2013 Nobody@Umbra Gaming Inc & Albag@Umbra Gaming Inc
*
*/

/* ScriptData
SDName: Boss_Erithol
SD%Complete: 100
SDComment: Tested in Game -> 100% -> Remade SQL with proper display ID's -> This is PTR Ready.
SDCategory: Karazhan
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_erithol.h"

class npc_event_arena_commander : public CreatureScript
{
public:
	npc_event_arena_commander() : CreatureScript("npc_event_arena_commander") { }

	bool OnGossipHello(Player * player, Creature * creature)
	{
		if(player->IsInCombat())
			return false;

		if(isBattleActive)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "I'm sorry, but a battle is already active.", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
		else
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, MSG_FIGHT_COMPUTER, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Nevermind", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player * player, Creature * creature, uint32 sender, uint32 actions)
	{
		if(sender != GOSSIP_SENDER_MAIN)
			return false;
		player->PlayerTalkClass->ClearMenus();
		switch(actions)
		{
		case GOSSIP_ACTION_INFO_DEF+1:

			m_PlayerGUID = player->GetGUID();
			playerName = player->GetName();
			isBattleActive = true;
			//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
			//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF+2:
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case GOSSIP_ACTION_INFO_DEF+3:
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}
		return true;
	}

	struct npc_event_arena_commanderAI : public ScriptedAI
	{
		npc_event_arena_commanderAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 checkBattle;
		uint32 checkPlayer;
		bool checkIsDead;
		bool resetOnce;

		void Reset()
		{
			events.Reset();
			player = NULL;
			summons.DespawnAll();
			checkIsDead = true;
			resetOnce = false;
			checkBattle = 2000;
			checkPlayer = 1000;
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			events.Update(diff);

			if(checkBattle <= diff)
			{
				if(!isBattleActive && m_PlayerGUID == 0 && !resetOnce)
				{
					events.Reset();
					resetOnce = true;
				}

				if(isBattleActive && m_PlayerGUID != 0 && resetOnce)
				{
					player = Unit::GetPlayer(*me, m_PlayerGUID);
					events.ScheduleEvent(EVENT_CHECK_ACTIVITY, 1000);
					resetOnce = false;
				}
				if(resetOnce)
					checkBattle = 2000;
			}
			else
				checkBattle -= diff;

			if(checkPlayer <= diff) // Checking battle as well
			{
				if(m_PlayerGUID == 0)
					return;

				if(hasLogged || !inZone)
				{
					isBattleActive = false;
					summons.DespawnAll();
					events.Reset();
					isWaveBossDead = 0;
					checkIsDead = true;
					hasLogged = false;
					inZone = true;
					resetOnce = false;
					player = NULL;
					m_PlayerGUID = NULL;
					playerName = "";
					sWorld->SendGlobalText("A challenger has been scared off and left the Erithol Battle Challenge! Who's next?", NULL);
				}
				checkPlayer = 1000;
			}
			else
				checkPlayer -= diff;

			if (isWaveBossDead == 1 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}
			else if (isWaveBossDead == 2 && !checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = true;
			}
			else if (isWaveBossDead == 3 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}
			else if (isWaveBossDead == 4 && !checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = true;
			}
			else if (isWaveBossDead == 5 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}
			else if (isWaveBossDead == 6 && !checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = true;
			}
			else if (isWaveBossDead == 7 && checkIsDead)
			{
				events.ScheduleEvent(EVENT_CHECK_WAVES, 1000);
				checkIsDead = false;
			}

			while(uint32 eventIds = events.ExecuteEvent())
			{
				switch(eventIds)
				{
				case EVENT_CHECK_ACTIVITY:
					{
						if(isBattleActive)
						{
							MessageOnWave(me, EVENT_CHECK_ACTIVITY);
							events.ScheduleEvent(EVENT_FIRST_WAVE, 10000);
						}
						else
							events.ScheduleEvent(EVENT_CHECK_ACTIVITY, 1000);
					}break;

				case EVENT_CHECK_WAVES:
					{
						if(!player)
							return;

						if(!isBattleActive)
						{
							summons.DespawnAll();
							checkIsDead = true;
							resetOnce = false;
							return;
						}

						int itemCount = player->GetItemCount(ITEM_INTRAVENOUS_HEALING_POTION);
						if(itemCount == 0)
							player->AddItem(ITEM_INTRAVENOUS_HEALING_POTION, 1); // Not really needed
						if(isWaveBossDead == 1)
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							AddEndRewards(player, 500, PVP_END_TOKEN, 1);
							events.ScheduleEvent(EVENT_FIRST_WAVE_TRASH, 25000);
							isWaveBossDead = 0; // This should be first wave Trash
						}

						if (isWaveBossDead == 2) // Spawn Event
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							AddEndRewards(player, 1000, PVP_END_TOKEN, 2);
							events.ScheduleEvent(EVENT_FIRST_WAVE_ELITE, 35000);
							isWaveBossDead = 0; // This should be first wave Elites
						}

						if(isWaveBossDead == 3) // 2nd Mini Boss
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							AddEndRewards(player, 2000, PVP_END_TOKEN, 5);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							events.ScheduleEvent(EVENT_SECOND_WAVE, 35000);
							isWaveBossDead = 0;
						}

						if(isWaveBossDead == 4) // sorcerers
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							AddEndRewards(player, 3000, PVP_END_TOKEN, 10);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							events.ScheduleEvent(EVENT_SECOND_WAVE_TRASH, 35000);
							isWaveBossDead = 0;
						}

						if(isWaveBossDead == 5) // The Unholys
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							AddEndRewards(player, 4000, PVP_END_TOKEN, 20);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							events.ScheduleEvent(EVENT_SECOND_WAVE_ELITE, 40000);
							isWaveBossDead = 0;
						}

						if(isWaveBossDead == 6) // The Riders of the Ice
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							AddEndRewards(player, 5000, PVP_END_TOKEN, 30);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
							events.ScheduleEvent(EVENT_FINAL_WAVE_BOSS, 35000);
							isWaveBossDead = 0;
						}

						if(isWaveBossDead == 7) // Dragon Final Event
						{
							MessageOnWave(me, EVENT_CHECK_WAVES);
							AddEndRewards(player, 10000, PVP_END_TOKEN, 50);
							//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
							events.ScheduleEvent(EVENT_COMPLETED_WAVES, 5000);
							isWaveBossDead = 0;
						}
					}break;

				case EVENT_FIRST_WAVE:
					sLog->outInfo(LOG_FILTER_GENERAL, "[Erithol Encounter]: Starting First Wave...");
					MessageOnWave(me, EVENT_FIRST_WAVE);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					me->SummonCreature(waveList[0], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_FIRST_WAVE_TRASH:
					MessageOnWave(me, EVENT_FIRST_WAVE_TRASH);
					me->SummonCreature(waveList[1], -10989.604f, -1911.566f, 78.868f, 4.583f, TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(NPC_PORTAL, m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					me->SummonCreature(NPC_PORTAL, m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					break;

				case EVENT_FIRST_WAVE_ELITE:
					MessageOnWave(me, EVENT_FIRST_WAVE_ELITE);
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					me->SummonCreature(waveList[8], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_SECOND_WAVE:
					MessageOnWave(me, EVENT_SECOND_WAVE);
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					me->SummonCreature(waveList[10], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_SECOND_WAVE_TRASH:
					MessageOnWave(me, EVENT_SECOND_WAVE_TRASH);
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					me->SummonCreature(waveList[13], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_SECOND_WAVE_ELITE:
					MessageOnWave(me, EVENT_SECOND_WAVE_ELITE);
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					me->SummonCreature(waveList[16], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_FINAL_WAVE_BOSS:
					sLog->outInfo(LOG_FILTER_GENERAL, "[Erithol Event]: Starting Final Wave...");
					MessageOnWave(me, EVENT_FINAL_WAVE_BOSS);
					player->PlayDirectSound(SOUND_HORN_WAVE_START);
					//player->CastSpell(player, SPELL_TELEPORT_VISUAL);
					//player->TeleportTo(532, -10989.599f, -1942.550f, 78.868f, 1.094f); // Maybe Remove this... Kinda Annoying
					me->SummonCreature(waveList[19], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation,
						TEMPSUMMON_MANUAL_DESPAWN, 0);
					break;

				case EVENT_COMPLETED_WAVES:
					me->MonsterYell("Congratulations to our finest Gladiator that went through a lot to earn our rewards! Who will be our next challenger?",
						LANG_UNIVERSAL, me->GetGUID());
					DoSendCompleteMessage(player->GetName());
					//player->TeleportTo(532, sTeleOut[0].m_positionX, sTeleOut[0].m_positionY, sTeleOut[0].m_positionZ, sTeleOut[0].m_orientation);
					isBattleActive = false;
					m_PlayerGUID = NULL;
					playerName = "";
					summons.DespawnAll();
					events.Reset();
					isWaveBossDead = 0;
					checkIsDead = true;
					break;
				}
			}
		}

		void JustSummoned(Creature * summoned)
		{
			summons.Summon(summoned);
		}
	private:
		EventMap events;
		Player * player;
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_event_arena_commanderAI(pCreature);
	}
};

class npc_erithol_event_demon_guard : public CreatureScript
{
public:
	npc_erithol_event_demon_guard() : CreatureScript("npc_erithol_event_demon_guard") { }

	struct npc_erithol_event_demon_guardAI : public ScriptedAI
	{
		npc_erithol_event_demon_guardAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 uiCharge;
		uint32 uiMortalStrike;
		uint32 uiCheckOutOfRange;
		bool spawnMinis;

		void Reset()
		{
			spawnMinis = true;
			uiCharge = 2000;
			uiMortalStrike = urand(5000, 8000);
			me->MonsterYell("You can't defeat me!", LANG_UNIVERSAL, me->GetGUID());
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 45233);
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			isWaveBossDead = 1;
			summons.DespawnAll();
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
			{
				summons.DespawnAll();
				return;
			}

			ScriptedAI::UpdateAI(diff);
			if(!isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}

			if(uiCharge <= diff)
			{
				me->CastSpell(me->GetVictim(), SPELL_BERSERKER_CHARGE);
				uiCharge = 12000;
			}
			else
				uiCharge -= diff;

			if(uiMortalStrike <= diff)
			{
				me->CastSpell(me->GetVictim(), SPELL_MORTAL_STRIKE);
				me->CastSpell(me->GetVictim(), SPELL_DEEP_WOUNDS); // Cast deep wounds after Mortal strike
				uiMortalStrike = urand(5000, 120000);
			}
			else
				uiMortalStrike -= diff;

			if(me->GetHealthPct() <= 45 && spawnMinis)
			{
				me->MonsterYell("Minions, come to my aid!", LANG_UNIVERSAL, me->GetGUID());
				me->SummonCreature(NPC_DEMON_GUARD_MINI, me->GetPositionX(), me->GetPositionY()+1, me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(NPC_DEMON_GUARD_MINI2, me->GetPositionX()+2, me->GetPositionY()+1, me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN, 0);
				me->SummonCreature(NPC_DEMON_GUARD_MINI3, me->GetPositionX()+2, me->GetPositionY()+1, me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN, 0);
				spawnMinis = false;
			}
			DoMeleeAttackIfReady();
		}
	private:
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_erithol_event_demon_guardAI(pCreature);
	}
};

class npc_demon_guard_mini : public CreatureScript
{
public:
	npc_demon_guard_mini() : CreatureScript("npc_demon_guard_mini") { }

	struct npc_demon_guard_miniAI : public ScriptedAI
	{
		npc_demon_guard_miniAI(Creature * c) : ScriptedAI(c) { me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID, 45233); }

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_demon_guard_miniAI(pCreature);
	}
};

class npc_wave_trigger : public CreatureScript
{
public:
	npc_wave_trigger() : CreatureScript("npc_wave_trigger") { }

	struct npc_wave_triggerAI : public ScriptedAI
	{
		npc_wave_triggerAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 startSpawnWave;
		int spawnPhase;

		void Reset()
		{
			startSpawnWave = 1000;
			spawnPhase = 0;
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!isBattleActive)
			{
				summons.DespawnAll();
				me->DespawnOrUnsummon(1);
				return;
			}

			if(startSpawnWave <= diff)
			{
				switch(spawnPhase)
				{
				case 0:
					{
						spawnPhase = 1;
						DoPortalSpawns();
						startSpawnWave = 13000;
					}break;

				case 1:
					{
						spawnPhase = 2;
						DoPortalSpawns();
						startSpawnWave = 13000;
					}break;

				case 2:
					{
						spawnPhase = 3;
						DoPortalSpawns();
						startSpawnWave = 13000;
					}break;

				case 3:
					{
						spawnPhase = 4;
						for(int i = 0; i < 2; i++)
							DoPortalSpawns();
						startSpawnWave = 25000;
					}break;

				case 4:
					{
						spawnPhase = 5;
						for(int i = 0; i < 2; i++)
							DoPortalSpawns();
						startSpawnWave = 25000;
					}break;

				case 5:
					{
						spawnPhase = 6;
						for(int i = 0; i < 4; i++)
							DoPortalSpawns();
						startSpawnWave = 36000;
					}break;

				case 6:
					{
						spawnPhase = 7;
						for(int i = 0; i < 4; i++)
							DoPortalSpawns();
						startSpawnWave = 36000;
					}break;

				case 7:
					{
						spawnPhase = 8;
						DoPortalSpawns();
						startSpawnWave = 10000;
					}break;

				case 8:
					{
						spawnPhase = 9;
						DoPortalSpawns();
						startSpawnWave = 10000;
					}break;

				case 9:
					{
						spawnPhase = 10;
						DoPortalSpawns();
						startSpawnWave = 15000;
					}break;

				case 10:
					{
						spawnPhase = 11;
						for(int i = 0; i < 4; i++)
							DoPortalSpawns();
						startSpawnWave = 60000;
					}break;

				case 11:
					{
						if(Player * player = Unit::GetPlayer(*me, m_PlayerGUID))
						{
							if(!player)
								return;
							me->PlayDirectSound(SOUND_WAVE_COMPLETE, player);
						}
						isWaveBossDead = 2;
						summons.DespawnAll();
						spawnPhase = 0;
						me->DespawnOrUnsummon(1);
					}break;
				}
			}
			else
				startSpawnWave -= diff;
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
			if(Player * player = Unit::GetPlayer(*me, m_PlayerGUID))
			{
				if(!player)
					return;
				summoned->SetInCombatWith(player);
				summoned->AI()->AttackStart(player);
				summoned->GetMotionMaster()->MoveChase(player, 500.0f);
			}
		}

		void DoPortalSpawns() // Spawns Random Npcs
		{
			int random = urand(0, 4);
			switch(random)
			{
			case 0:
				me->SummonCreature(waveList[2], m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 1:
				me->SummonCreature(waveList[3], m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 2:
				me->SummonCreature(waveList[4], m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 3:
				me->SummonCreature(waveList[5], m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 4:
				me->SummonCreature(waveList[6], m_WaveSpawns[1].m_positionX, m_WaveSpawns[1].m_positionY, m_WaveSpawns[1].m_positionZ, m_WaveSpawns[1].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;
			}
			int random2 = urand(0, 4);
			switch(random2)
			{
			case 0:
				me->SummonCreature(waveList[2], m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 1:
				me->SummonCreature(waveList[3], m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 2:
				me->SummonCreature(waveList[4], m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 3:
				me->SummonCreature(waveList[5], m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;

			case 4:
				me->SummonCreature(waveList[6], m_WaveSpawns[2].m_positionX, m_WaveSpawns[2].m_positionY, m_WaveSpawns[2].m_positionZ, m_WaveSpawns[2].m_orientation,
					TEMPSUMMON_MANUAL_DESPAWN, 0);
				break;
			}
		}
	private:
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_wave_triggerAI(pCreature);
	}
};

class npc_wave_spawns : public CreatureScript
{
public:
	npc_wave_spawns() : CreatureScript("npc_wave_spawns") { }

	struct npc_wave_spawnsAI : public ScriptedAI
	{
		npc_wave_spawnsAI(Creature * c) : ScriptedAI(c) { }

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!isBattleActive)
				me->DespawnOrUnsummon(1);

			if(isWaveBossDead == 2)
				me->DespawnOrUnsummon(1);

			if(!UpdateVictim())
				return;

			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_wave_spawnsAI(pCreature);
	}
};

class npc_wave_portals : public CreatureScript
{
public:
	npc_wave_portals() : CreatureScript("npc_wave_portals") { }

	struct npc_wave_portalsAI : public ScriptedAI
	{
		npc_wave_portalsAI(Creature * c) : ScriptedAI(c) { }

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!isBattleActive)
				me->DespawnOrUnsummon(1);

			if(isWaveBossDead == 2)
				me->DespawnOrUnsummon(1);
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_wave_portalsAI(pCreature);
	}
};

uint32 tankAuras[] = { SPELL_BATTLE_STANCE, SPELL_INCREASE_BLOCK_VALUE, SPELL_INCREASE_DEFENSE_RATING, SPELL_INCREASE_DODGE_RATING };
class npc_xiombarg_the_tank : public CreatureScript
{
public:
	npc_xiombarg_the_tank() : CreatureScript("npc_xiombarg_the_tank") { }

	struct npc_xiombarg_the_tankAI : public ScriptedAI
	{
		npc_xiombarg_the_tankAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 shoutBuff;
		uint32 groundStomp;
		uint32 uiExecute;
		uint32 tankStrike;
		uint32 uiRend;
		uint32 shieldBlock;
		uint32 shieldBash;

		void Reset()
		{
			me->MonsterYell("You will all die by my blades!!!", LANG_UNIVERSAL, me->GetGUID());
			summons.DespawnAll();
			me->SummonCreature(waveList[9], me->GetPositionX(), me->GetPositionY()+3, me->GetPositionZ(), me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN, 0);
			shoutBuff = 1000;
			groundStomp = 6000;
			shieldBash = urand(9000, 13000);
			uiExecute = urand(21000, 26000);
			shieldBlock = 18000;
			tankStrike = urand(4000, 8000);
			uiRend = 11000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51516);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51533);
			for(int i = 0; i < 4; i++)
				me->AddAura(tankAuras[i], me);
		}

		void EnterCombat(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoCast(who, SPELL_TANK_CHARGE);
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			summons.DespawnAll();
			isWaveBossDead = 3;
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;

			if(!isBattleActive)
			{
				summons.DespawnAll();
				me->DespawnOrUnsummon(1);
				return;
			}

			if(shoutBuff <= diff)
			{
				me->AddAura(SPELL_SHOUT_BUFF, me);
				shoutBuff = 120000; // 2 minutes
			}
			else
				shoutBuff -= diff;

			if(groundStomp <= diff)
			{
				DoCast(me->GetVictim(), SPELL_GROUND_STOMP, true);
				groundStomp = 6000;
			}
			else
				groundStomp -= diff;

			if(uiExecute <= diff)
			{
				if(me->GetVictim()->GetHealthPct() <= 20)
					DoCast(me->GetVictim(), SPELL_EXECUTE);
				uiExecute = urand(21000, 26000);
			}
			else
				uiExecute -= diff;

			if(shieldBlock <= diff)
			{
				DoCast(me, SPELL_SHIELD_BLOCK); // Cast on self
				shieldBlock = 18000;
			}
			else
				shieldBlock -= diff;

			if(tankStrike <= diff)
			{
				DoCast(me->GetVictim(), SPELL_TANK_STRIKE);
				tankStrike = urand(4000, 8000);
			}
			else
				tankStrike -= diff;

			if(uiRend <= diff)
			{
				DoCast(me->GetVictim(), SPELL_REND, true);
				uiRend = 15000;
			}
			else
				uiRend -= diff;
			DoMeleeAttackIfReady();
		}
	private:
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_xiombarg_the_tankAI(pCreature);
	}
};

class npc_field_medic : public CreatureScript
{
public:
	npc_field_medic() : CreatureScript("npc_field_medic") { }

	struct npc_field_medicAI : public ScriptedAI
	{
		npc_field_medicAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiRegrowthTimer;
		uint32 uiHealingTouchTimer;
		uint32 uiHealRejuvTimer;
		uint32 uiFlashHealTimer;
		uint32 uiPowerShieldTimer;
		uint32 uiLifebloomTimer;
		int casted;
		bool showOnce;
		bool canHealMaster;

		void Reset()
		{
			uiRegrowthTimer = urand(11000, 13000);
			uiHealingTouchTimer = urand(6000, 8000);
			uiHealRejuvTimer = 15000;
			uiFlashHealTimer = urand(14000, 17000);
			uiPowerShieldTimer = 1000;
			uiLifebloomTimer = 2000;
			canHealMaster = true;
			showOnce = false;
			int casted = 0;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51404);
			me->AddAura(SPELL_HEAL_AURA_HOTS, me);
			xiom = NULL;
			xiom = me->FindNearestCreature(NPC_XIOMBARG_THE_TANK, 50.0f, true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!xiom)
				return;

			if(xiom && xiom->isDead() && !showOnce)
			{
				me->MonsterYell("Master Dead?!!! I give up!...", LANG_UNIVERSAL, me->GetGUID());
				me->setFaction(35);
				me->DespawnOrUnsummon(1);
				showOnce = true;
				return;
			}

			if(xiom && !xiom->IsInCombat())
				return;

			if(xiom && xiom->GetHealthPct() >= 100)
				canHealMaster = false;
			else
				canHealMaster = true;

			if(uiRegrowthTimer <= diff)
			{
				if(canHealMaster)
					DoCast(xiom, SPELL_HEAL_REGROWTH, true);
				if(me->GetHealthPct() <= 70)
					DoCast(me, SPELL_HEAL_REGROWTH, true);
				uiRegrowthTimer = urand(10000, 16000);
			}
			else
				uiRegrowthTimer -= diff;

			if(uiHealingTouchTimer <= diff)
			{
				if(xiom->GetHealthPct() <= 45 && canHealMaster)
					DoCast(xiom, SPELL_HEAL_NORMAL, true);
				if(me->GetHealthPct() <= 30)
					DoCast(me, SPELL_HEAL_NORMAL, true);
				uiHealingTouchTimer = urand(6000, 8000);
			}
			else
				uiHealingTouchTimer -= diff;

			if(uiHealRejuvTimer <= diff)
			{
				if(canHealMaster)
					DoCast(xiom, SPELL_HEAL_REJUV, true);
				if(me->GetHealthPct() <= 90)
					DoCast(me, SPELL_HEAL_REJUV, true);
				uiHealRejuvTimer = 15000;
			}
			else
				uiHealRejuvTimer -= diff;

			if(uiFlashHealTimer <= diff)
			{
				if(canHealMaster)
					DoCast(xiom, SPELL_FLASH_HEAL, true);
				if(me->GetHealthPct() <= 55)
					DoCast(me, SPELL_FLASH_HEAL, true);
				uiFlashHealTimer = urand(14000, 17000);
			}
			else
				uiFlashHealTimer -= diff;

			if(uiPowerShieldTimer <= diff)
			{
				DoCast(me, SPELL_POWER_WORD_SHIELD);
				uiPowerShieldTimer = 30000;
			}
			else
				uiPowerShieldTimer -= diff;

			if(uiLifebloomTimer <= diff)
			{
				if(canHealMaster)
				{
					DoCast(xiom, SPELL_HEAL_LIFEBLOOM, true); // Stacking
					casted++; // Increment the value
					if(casted != 3)
						uiLifebloomTimer = 2000;
					else // Prevents further stacking of this spell
					{
						uiLifebloomTimer = 26000;
						casted = 0;
					}
				}
			}
			else
				uiLifebloomTimer -= diff;
		}
	private:
		Creature * xiom;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_field_medicAI(pCreature);
	}
};

class npc_main_sorc : public CreatureScript
{
public:
	npc_main_sorc() : CreatureScript("npc_main_sorc") { }

	struct npc_main_sorcAI : public ScriptedAI
	{
		npc_main_sorcAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 uiFireballTimer;
		uint32 uiFlamestrikeTimer;
		uint32 uiIceboltTimer;
		uint32 uiRainoffireTimer;
		uint32 uiBlizzardTimer;

		void Reset()
		{
			me->MonsterYell("We'll teach you a valued lesson.", LANG_UNIVERSAL, me->GetGUID());

			uiFireballTimer = 4000;
			uiFlamestrikeTimer = 6000;
			uiIceboltTimer = 4500;
			uiRainoffireTimer = 10000;
			uiBlizzardTimer = 12000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51517);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51441);
			summons.DespawnAll();
			me->SummonCreature(waveList[11], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY+3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
			me->SummonCreature(waveList[12], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY-3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			summons.DespawnAll();
			isWaveBossDead = 4;
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;

			if(!isBattleActive)
			{
				summons.DespawnAll();
				me->DespawnOrUnsummon(1);
				return;
			}

			if(uiFireballTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FIREBALL);
				uiFireballTimer = 4000;
			}   else uiFireballTimer -= diff;

			if(uiFlamestrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FLAMESTRIKE);
				uiFlamestrikeTimer = 6000;
			}   else uiFlamestrikeTimer -= diff;

			if(uiIceboltTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_ICEBOLT);
				uiIceboltTimer = 4500;
			}   else uiIceboltTimer -= diff;

			if(uiRainoffireTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_RAINOFFIRE);
				uiRainoffireTimer = 10000;
			}   else uiRainoffireTimer -= diff;

			if(uiBlizzardTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_BLIZZARD);
				uiBlizzardTimer = 12000;
			}   else uiBlizzardTimer -= diff;

			DoMeleeAttackIfReady();
		}
	private:
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_main_sorcAI(pCreature);
	}
};

class npc_sorc_initiate : public CreatureScript
{
public:
	npc_sorc_initiate() : CreatureScript("npc_sorc_initiate") { }

	struct npc_sorc_initiateAI : public ScriptedAI
	{
		npc_sorc_initiateAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiFireballTimer;
		uint32 uiFlamestrikeTimer;
		uint32 uiIceboltTimer;
		uint32 uiRainoffireTimer;
		uint32 uiBlizzardTimer;

		void Reset()
		{
			uiFireballTimer = 4000;
			uiFlamestrikeTimer = 6000;
			uiIceboltTimer = 5000;
			uiRainoffireTimer = 12000;
			uiBlizzardTimer = 10000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51517);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51441);
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;

			if(uiFireballTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FIREBALL);
				uiFireballTimer = 4000;
			}   else uiFireballTimer -= diff;

			if(uiFlamestrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FLAMESTRIKE);
				uiFlamestrikeTimer = 6000;
			}   else uiFlamestrikeTimer -= diff;

			if(uiIceboltTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_ICEBOLT);
				uiIceboltTimer = 5000;
			}   else uiIceboltTimer -= diff;

			if(uiRainoffireTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_RAINOFFIRE);
				uiRainoffireTimer = 12000;
			}   else uiRainoffireTimer -= diff;

			if(uiBlizzardTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_BLIZZARD);
				uiBlizzardTimer = 10000;
			}   else uiBlizzardTimer -= diff;

			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_sorc_initiateAI(pCreature);
	}
};

class npc_main_unholy : public CreatureScript
{
public:
	npc_main_unholy() : CreatureScript("npc_main_unholy") { }

	struct npc_main_unholyAI : public ScriptedAI
	{
		npc_main_unholyAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 uiCinematic;
		uint32 uiAoeTimer;
		uint32 uiPlagueStrikeTimer;
		uint32 uiStangulateTimer;
		uint32 uiDeathStrikeTimer;
		uint32 uiArmyOfTheDeadTimer;
		int cinematicPassed;
		bool checkGuin;
		bool boneArmor;

		void Reset()
		{
			me->MonsterSay("What is this? A trap?", LANG_UNIVERSAL, me->GetGUID());
			uiCinematic = 9000;
			cinematicPassed = 0;
			checkGuin = true;
			boneArmor = true;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51393);
			summons.DespawnAll();
			twin = me->SummonCreature(waveList[14], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY+3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
			guin = me->SummonCreature(waveList[15], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY-3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
			twin = NULL;
			guin = NULL;
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			summons.DespawnAll();
			twin = NULL;
			guin = NULL;
			isWaveBossDead = 5;
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!isBattleActive)
			{
				summons.DespawnAll();
				twin = NULL;
				guin = NULL;
				me->DespawnOrUnsummon(1);
				return;
			}

			if(uiCinematic <= diff)
			{
				switch(cinematicPassed)
				{
				case 0:
					me->MonsterSay("Ahh, so this thing wants to fight, huh?", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 1;
					uiCinematic = 10000;
					break;

				case 1:
					me->MonsterSay("Don't be a fool!", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 2;
					uiCinematic = 9000;
					break;

				case 2:
					me->MonsterSay("Twin, no! Let him go. We can just watch, for now....", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 3;
					break;
				}
			}
			else
				uiCinematic -= diff;

			if(checkGuin)
			{
				if(guin && guin->isDead())
				{
					me->MonsterYell("SO BE IT!", LANG_UNIVERSAL, me->GetGUID());
					me->AddAura(SPELL_UNHOLY_BONE_SHIELD, me);
					me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
					uiAoeTimer = 25000;
					uiPlagueStrikeTimer = urand(5000, 8000);
					uiDeathStrikeTimer = urand(9000, 12000);
					uiStangulateTimer = 15000;
					uiArmyOfTheDeadTimer = 30000;
					me->SetReactState(REACT_AGGRESSIVE);
					if(twin)
					{
						twin->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
						twin->SetReactState(REACT_AGGRESSIVE);
					}
					checkGuin = false;
				}
			}

			if(!UpdateVictim())
				return;

			if(HealthBelowPct(25) && boneArmor)
			{
				me->AddAura(SPELL_UNHOLY_BONE_SHIELD, me);
				boneArmor = false;
			}

			if(uiAoeTimer <= diff)
			{
				me->CastSpell(me->GetVictim()->GetPositionX(), me->GetVictim()->GetPositionY(), me->GetVictim()->GetPositionZ(), SPELL_UNHOLY_AOE, true);
				uiAoeTimer = 25000;
			}
			else
				uiAoeTimer -= diff;

			if(uiPlagueStrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_PLAGUE_STRIKE);
				uiPlagueStrikeTimer = urand(5000, 8000);
			}
			else
				uiPlagueStrikeTimer -= diff;

			if(uiStangulateTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_STANGULATE, true);
				uiStangulateTimer = 15000;
			}
			else
				uiStangulateTimer -= diff;

			if(uiDeathStrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_DEATH_STRIKE);
				uiDeathStrikeTimer = urand(9000, 12000);
			}
			else
				uiDeathStrikeTimer -= diff;

			if(uiArmyOfTheDeadTimer <= diff)
			{
				DoCast(me, SPELL_UNHOLY_ARMY, true);
				uiArmyOfTheDeadTimer = 30000;
			}
			else
				uiArmyOfTheDeadTimer -= diff;
			DoMeleeAttackIfReady();
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

	private:
		Creature * guin;
		Creature * twin;
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_main_unholyAI(pCreature);
	}
};

class npc_unholy_twin : public CreatureScript
{
public:
	npc_unholy_twin() : CreatureScript("npc_unholy_twin") { }

	struct npc_unholy_twinAI : public ScriptedAI
	{
		npc_unholy_twinAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiCinematic;
		uint32 uiAoeTimer;
		uint32 uiPlagueStrikeTimer;
		uint32 uiStangulateTimer;
		uint32 uiDeathStrikeTimer;
		uint32 uiArmyOfTheDeadTimer;
		int cinematicPassed;
		bool boneArmor;

		void Reset()
		{
			uiCinematic = 6000;
			boneArmor = true;
			cinematicPassed = 0;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51393);
		}

		void OnCombatStart(Unit * /* who */) OVERRIDE
		{
			me->AddAura(SPELL_UNHOLY_BONE_SHIELD, me);
			uiAoeTimer = 25000;
			uiPlagueStrikeTimer = urand(5000, 8000);
			uiDeathStrikeTimer = urand(9000, 12000);
			uiStangulateTimer = 15000;
			uiArmyOfTheDeadTimer = 30000;
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(uiCinematic <= diff)
			{
				switch(cinematicPassed)
				{
				case 0:
					me->MonsterSay("I believe it wants to fight us.", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 1;
					uiCinematic = 7000;
					break;

				case 1:
					me->MonsterSay("Yes. So it seems.", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 2;
					uiCinematic = 6000;
					break;

				case 2:
					me->MonsterSay("What? FIGHT?", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 3;
					uiCinematic = 3000;
					break;

				case 3:
					me->MonsterYell("Murfaz! NO!", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 4;
					break;
				}
			}
			else
				uiCinematic -= diff;

			if(!UpdateVictim())
				return;

			if(HealthBelowPct(25) && boneArmor)
			{
				me->AddAura(SPELL_UNHOLY_BONE_SHIELD, me);
				boneArmor = false;
			}

			if(uiAoeTimer <= diff)
			{
				me->CastSpell(me->GetVictim()->GetPositionX(), me->GetVictim()->GetPositionY(), me->GetVictim()->GetPositionZ(), SPELL_UNHOLY_AOE, true);
				uiAoeTimer = 25000;
			}
			else
				uiAoeTimer -= diff;

			if(uiPlagueStrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_PLAGUE_STRIKE);
				uiPlagueStrikeTimer = urand(5000, 8000);
			}
			else
				uiPlagueStrikeTimer -= diff;

			if(uiStangulateTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_STANGULATE, true);
				uiStangulateTimer = 15000;
			}
			else
				uiStangulateTimer -= diff;

			if(uiDeathStrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_UNHOLY_DEATH_STRIKE);
				uiDeathStrikeTimer = urand(9000, 12000);
			}
			else
				uiDeathStrikeTimer -= diff;

			if(uiArmyOfTheDeadTimer <= diff)
			{
				DoCast(me, SPELL_UNHOLY_ARMY, true);
				uiArmyOfTheDeadTimer = 30000;
			}
			else
				uiArmyOfTheDeadTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_unholy_twinAI(pCreature);
	}
};

class npc_unholy_pet : public CreatureScript
{
public:
	npc_unholy_pet() : CreatureScript("npc_unholy_pet") { }

	struct npc_unholy_petAI : public ScriptedAI
	{
		npc_unholy_petAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiCinematicTimer;
		uint32 uiPhaseChangeTimer;
		uint32 uiEnrageTimer;
		int phase;
		int cinematicPassed;

		void Reset()
		{
			uiCinematicTimer = 16000;
			uiPhaseChangeTimer = 1000;
			uiEnrageTimer = 5000;
			phase = 1;
			cinematicPassed = 0;
			me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
			me->SetReactState(REACT_PASSIVE);
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(uiCinematicTimer <= diff)
			{
				switch(cinematicPassed)
				{
				case 0:
					me->MonsterSay("Why we no fight masters?", LANG_UNIVERSAL, me->GetGUID());
					cinematicPassed = 1;
					uiCinematicTimer = 5000;
					break;

				case 1:
					me->MonsterYell("Hrm, I hungry, I fight!", LANG_UNIVERSAL, me->GetGUID());
					me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NOT_SELECTABLE);
					me->SetReactState(REACT_AGGRESSIVE);
					if(Player * player = me->SelectNearestPlayer(500.0f))
						if(player && player->GetGUID() == m_PlayerGUID)
						{
							me->Attack(player, true);
							me->GetMotionMaster()->MoveChase(player);
						}
						cinematicPassed = 2;
						break;
				}
			}
			else
				uiCinematicTimer -= diff;

			if(!UpdateVictim())
				return;

			if(uiPhaseChangeTimer <= diff)
			{
				switch(phase)
				{
				case 1:
					if(HealthBelowPct(85))
					{
						DoIncreaseHealth(me, 2.0f);
						phase = 2;
					}
					break;

				case 2:
					if(HealthBelowPct(65))
					{
						DoIncreaseHealth(me, 2.5f);
						phase = 3;
					}
					break;

				case 3:
					if(HealthBelowPct(35))
					{
						DoIncreaseHealth(me, 3.0f);
						phase = 4;
					}
					break;

				case 4:
					if(HealthBelowPct(15))
					{
						me->MonsterYell("HUNGER!", LANG_UNIVERSAL, me->GetGUID());
						DoIncreaseHealth(me, 3.5f);
						phase = 5;
					}
					break;
				}
			}
			else
				uiPhaseChangeTimer -= diff;

			if(uiEnrageTimer <= diff)
			{
				DoCast(SPELL_ENRAGE);
				uiEnrageTimer = 5000;
			}
			else
				uiEnrageTimer -= diff;
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_unholy_petAI(pCreature);
	}
};

class npc_army_ghoul : public CreatureScript
{
public:
	npc_army_ghoul() : CreatureScript("npc_army_ghoul") { }

	struct npc_army_ghoulAI : public ScriptedAI
	{
		npc_army_ghoulAI(Creature * c) : ScriptedAI(c) { }

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}
	};
	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_army_ghoulAI(pCreature);
	}
};

class npc_shaman_guardian : public CreatureScript
{
public:
	npc_shaman_guardian() : CreatureScript("npc_shaman_guardian") { }

	struct npc_shaman_guardianAI : public ScriptedAI
	{
		npc_shaman_guardianAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 uiFireballTimer;
		uint32 uiFlamestrikeTimer;
		uint32 uiIceboltTimer;
		uint32 uiRainoffireTimer;
		uint32 uiBlizzardTimer;

		void Reset()
		{
			me->MonsterYell("Now you will face our wrath!", LANG_UNIVERSAL, me->GetGUID());

			uiFireballTimer = 4000;
			uiFlamestrikeTimer = 6000;
			uiIceboltTimer = 4500;
			uiRainoffireTimer = 10000;
			uiBlizzardTimer = 12000;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51517);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51441);
			summons.DespawnAll();
			me->SummonCreature(waveList[17], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY+3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
			me->SummonCreature(waveList[18], m_WaveSpawns[0].m_positionX, m_WaveSpawns[0].m_positionY-3, m_WaveSpawns[0].m_positionZ, m_WaveSpawns[0].m_orientation, TEMPSUMMON_MANUAL_DESPAWN, 0);
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			isWaveBossDead = 6;
			summons.DespawnAll();
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;

			if(!isBattleActive)
			{
				summons.DespawnAll();
				me->DespawnOrUnsummon(1);
				return;
			}

			if(uiFireballTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FIREBALL);
				uiFireballTimer = 4000;
			}   else uiFireballTimer -= diff;

			if(uiFlamestrikeTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_FLAMESTRIKE);
				uiFlamestrikeTimer = 6000;
			}   else uiFlamestrikeTimer -= diff;

			if(uiIceboltTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_ICEBOLT);
				uiIceboltTimer = 4500;
			}   else uiIceboltTimer -= diff;

			if(uiRainoffireTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_RAINOFFIRE);
				uiRainoffireTimer = 10000;
			}   else uiRainoffireTimer -= diff;

			if(uiBlizzardTimer <= diff)
			{
				DoCast(me->GetVictim(), SPELL_SORC_BLIZZARD);
				uiBlizzardTimer = 12000;
			}   else uiBlizzardTimer -= diff;

			DoMeleeAttackIfReady();
		}
	private:
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_shaman_guardianAI(pCreature);
	}
};

class npc_shaman_brute : public CreatureScript
{
public:
	npc_shaman_brute() : CreatureScript("npc_shaman_brute") { }

	struct npc_shaman_bruteAI : public ScriptedAI
	{
		npc_shaman_bruteAI(Creature * c) : ScriptedAI(c) { }

		uint32 uiLightningTimer;
		uint32 uiChainLightningTimer;
		uint32 uiMoonfireTimer;
		uint32 uiLightningStormTimer;
		uint32 uiLightningWaitTimer;
		bool InLightningChannel;

		void Reset()
		{
			uiLightningTimer = 13000;
			uiChainLightningTimer = urand(6000, 9000);
			uiMoonfireTimer = 5000;
			uiLightningStormTimer = urand(20000, 26000);
			InLightningChannel = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 51517);
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+1, 51447);
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim())
				return;

			if(!isBattleActive)
			{
				me->DespawnOrUnsummon(1);
				return;
			}

			if(uiLightningTimer <= diff)
			{
				if(!InLightningChannel)
					DoCast(me->GetVictim(), SPELL_SHAMAN_LIGHTNING);
				uiLightningTimer = 13000;
			}
			else
				uiLightningTimer -= diff;

			if(uiChainLightningTimer <= diff)
			{
				if(!InLightningChannel)
					DoCast(me->GetVictim(), SPELL_SHAMAN_CHAIN_LIGHTNING, true);
				uiChainLightningTimer = urand(6000, 9000);
			}
			else
				uiChainLightningTimer -= diff;

			if(uiMoonfireTimer <= diff)
			{
				if(!InLightningChannel)
					DoCast(me->GetVictim(), SPELL_SHAMAN_MOONFIRE);
				uiMoonfireTimer = 5000;
			}
			else
				uiMoonfireTimer -= diff;

			if(uiLightningStormTimer <= diff)
			{
				if(!InLightningChannel)
				{
					me->CastSpell(me->GetVictim(), SPELL_SHAMAN_STORM, true);
					InLightningChannel = true;
					uiLightningWaitTimer = 10000;
				}
				uiLightningStormTimer = urand(20000, 26000);
			}
			else
				uiLightningStormTimer -= diff;

			if(InLightningChannel)
			{
				if(uiLightningWaitTimer <= diff)
				{
					InLightningChannel = false;
				}
				else
					uiLightningWaitTimer -= diff;
			}

			if(!InLightningChannel)
				DoMeleeAttackIfReady();
		}
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_shaman_bruteAI(pCreature);
	}
};

class npc_erithol_final : public CreatureScript
{
public:
	npc_erithol_final() : CreatureScript("npc_erithol_final") { }

	struct npc_erithol_finalAI : public ScriptedAI
	{
		npc_erithol_finalAI(Creature * c) : ScriptedAI(c), summons(me) { }

		uint32 uiBerserkTimer;
		uint32 uiBreathTimer;
		uint32 uiTailWhipTimer;
		uint32 uiClawTimer;
		uint32 uiFlameStrikeTimer;
		uint32 uiFlightTimer;
		uint32 uiFlightWaitTimer;
		uint32 uiLandTimer;

		uint32 m_Phase;
		int  FlameStrikeData;
		bool IsInFlight;
		bool FireShield;
		bool canLand;

		void Reset()
		{
			if(!IsCombatMovementAllowed())
				SetCombatMovement(true);

			uiFlightTimer = 10000; // 10s
			uiClawTimer = urand(3000, 5000);
			uiTailWhipTimer = urand(12000, 16000);
			uiBreathTimer = urand(18000, 22000);

			FlameStrikeData = 0;

			summons.DespawnAll();
			IsInFlight = false;
			FireShield = true;
			canLand = false;
		}

		void EnterCombat(Unit * /* who */) OVERRIDE
		{
			m_Phase = PHASE_START_COMBAT;
			uiBerserkTimer = 420000; // 7 Mins
		}

		void KilledUnit(Unit * who) OVERRIDE
		{
			if(who && who->GetTypeId() != TYPEID_PLAYER)
				return;
			DoEndBattle(me, summons);
		}

		void JustDied(Unit * killer) OVERRIDE
		{
			if(killer && killer->GetTypeId() != TYPEID_PLAYER &&
				killer->GetGUID() != m_PlayerGUID)
				return;
			me->PlayDirectSound(SOUND_WAVE_COMPLETE, killer->ToPlayer());
			m_Phase = PHASE_END;
			isWaveBossDead = 7;
		}

		void MovementInform(uint32 type, uint32 id) OVERRIDE
		{
			if(type == POINT_MOTION_TYPE)
			{
				switch(id)
				{
				case 1:
					me->SetFacingTo(sMoveData[0].o);
					me->SetDisableGravity(true);
					break;
				}
			}
		}

		void JustSummoned(Creature * summoned) OVERRIDE
		{
			summons.Summon(summoned);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if(!UpdateVictim() || m_Phase == PHASE_END)
				return;

			if(!isBattleActive)
			{
				summons.DespawnAll();
				m_Phase = PHASE_END;
				me->DespawnOrUnsummon(1);
				return;
			}

			if(m_Phase == PHASE_START_COMBAT)
			{
				if(HealthBelowPct(15) && FireShield) // Doesn't matter if you're flying
				{
					DoCast(me, SPELL_DRAGON_FIRE_SHIELD, true);
					FireShield = false;
				}

				if(uiClawTimer <= diff)
				{
					if(!IsInFlight)
						DoCast(me->GetVictim(), SPELL_DRAGON_CLAW);
					uiClawTimer = urand(3000, 5000);
				}
				else
					uiClawTimer -= diff;

				if(uiBreathTimer <= diff)
				{
					if(!IsInFlight)
						me->CastSpell(me->GetVictim(), SPELL_DRAGON_BREATH, true);
					uiBreathTimer = urand(18000, 24000);
				}
				else
					uiBreathTimer -= diff;

				if(uiTailWhipTimer <= diff)
				{
					if(!IsInFlight)
						DoCastAOE(SPELL_DRAGON_TAIL_WHIP, false);
					uiTailWhipTimer = urand(12000, 16000);
				}
				else
					uiTailWhipTimer -= diff;

				if(uiBerserkTimer <= diff)
				{
					DoCast(me, SPELL_DRAGON_BERSERK);
					uiBerserkTimer = 420000;
				}
				else
					uiBerserkTimer -= diff;

				if(uiFlightTimer <= diff && !IsInFlight)
				{
					me->SetCanFly(true);
					me->SetSpeed(MOVE_FLIGHT, 1.3f);
					me->GetMotionMaster()->MovePoint(532, -11013.151f, -1908.844f, 94.564f);
					FlameStrikeData = 0;
					uiFlameStrikeTimer = 6000;
					uiFlightWaitTimer = 30000;
					IsInFlight = true;
					uiFlightTimer = 50000;
				}
				else
					uiFlightTimer -= diff;

				if(IsInFlight)
				{
					if(uiFlameStrikeTimer <= diff)
					{
						switch(FlameStrikeData)
						{
						case 0:
							me->MonsterTextEmote("The Massive Demon has a lot of fire inside!", 0, true);
							groundTarget = me->SummonGameObject(sMoveData[0].gobject, me->GetVictim()->GetPositionX()-3, me->GetVictim()->GetPositionY()+6, me->GetVictim()->GetPositionZ(),
								me->GetOrientation(), 0, 0, 0, 0, 0);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 1;
							break;

						case 1:
							trigger = me->SummonCreature(NPC_ERITHOL_BOSS_TRIGGER, groundTarget->GetPositionX(), groundTarget->GetPositionY(), groundTarget->GetPositionZ(), 0.0f,
								TEMPSUMMON_TIMED_DESPAWN, 3000);
							me->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), sMoveData[0].spellId, true);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 2;
							break;

						case 2:
							if(groundTarget)
								groundTarget->Delete();
							me->MonsterTextEmote("The Massive Demon has a lot of fire inside!", 0, true);
							groundTarget = me->SummonGameObject(sMoveData[0].gobject, me->GetVictim()->GetPositionX()-3, me->GetVictim()->GetPositionY()-6, me->GetVictim()->GetPositionZ(),
								me->GetOrientation(), 0, 0, 0, 0, 0);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 3;
							break;

						case 3:
							trigger = me->SummonCreature(NPC_ERITHOL_BOSS_TRIGGER, groundTarget->GetPositionX(), groundTarget->GetPositionY(), groundTarget->GetPositionZ(), 0.0f,
								TEMPSUMMON_TIMED_DESPAWN, 3000);
							me->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), sMoveData[0].spellId, true);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 4;
							break;

						case 4:
							if(groundTarget)
								groundTarget->Delete();
							me->MonsterTextEmote("The Massive Demon has a lot of fire inside!", 0, true);
							groundTarget = me->SummonGameObject(sMoveData[0].gobject, me->GetVictim()->GetPositionX()-3, me->GetVictim()->GetPositionY()-6, me->GetVictim()->GetPositionZ(),
								me->GetOrientation(), 0, 0, 0, 0, 0);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 5;
							break;

						case 5:
							trigger = me->SummonCreature(NPC_ERITHOL_BOSS_TRIGGER, groundTarget->GetPositionX(), groundTarget->GetPositionY(), groundTarget->GetPositionZ(), 0.0f,
								TEMPSUMMON_TIMED_DESPAWN, 3000);
							me->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), sMoveData[0].spellId, true);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 6;
							break;

						case 6:
							if(groundTarget)
								groundTarget->Delete();
							me->MonsterTextEmote("The Massive Demon has a lot of fire inside!", 0, true);
							groundTarget = me->SummonGameObject(sMoveData[0].gobject, me->GetVictim()->GetPositionX()-3, me->GetVictim()->GetPositionY()+6, me->GetVictim()->GetPositionZ(),
								me->GetOrientation(), 0, 0, 0, 0, 0);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 7;
							break;

						case 7:
							trigger = me->SummonCreature(NPC_ERITHOL_BOSS_TRIGGER, groundTarget->GetPositionX(), groundTarget->GetPositionY(), groundTarget->GetPositionZ(), 0.0f,
								TEMPSUMMON_TIMED_DESPAWN, 3000);
							me->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), sMoveData[0].spellId, true);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 8;
							break;

						case 8:
							if(groundTarget)
								groundTarget->Delete();
							me->MonsterTextEmote("The Massive Demon has a lot of fire inside!", 0, true);
							groundTarget = me->SummonGameObject(sMoveData[0].gobject, me->GetVictim()->GetPositionX()-3, me->GetVictim()->GetPositionY()+6, me->GetVictim()->GetPositionZ(),
								me->GetOrientation(), 0, 0, 0, 0, 0);
							uiFlameStrikeTimer = 2000;
							FlameStrikeData = 9;
							break;

						case 9:
							trigger = me->SummonCreature(NPC_ERITHOL_BOSS_TRIGGER, groundTarget->GetPositionX(), groundTarget->GetPositionY(), groundTarget->GetPositionZ(), 0.0f,
								TEMPSUMMON_TIMED_DESPAWN, 3000);
							me->CastSpell(trigger->GetPositionX(), trigger->GetPositionY(), trigger->GetPositionZ(), sMoveData[0].spellId, true);
							uiFlameStrikeTimer = 1000;
							FlameStrikeData = 10;
							break;

						case 10:
							if(groundTarget)
								groundTarget->Delete();
							FlameStrikeData = 11;
							break;
						}
					}
					else
						uiFlameStrikeTimer -= diff;

					if(uiFlightWaitTimer <= diff && !canLand)
					{
						me->GetMotionMaster()->MovePoint(2, me->GetHomePosition()); // Back Home
						canLand = true;
						uiLandTimer = 5000;
					}
					else
						uiFlightWaitTimer -= diff;

					if(uiLandTimer <= diff && canLand)
					{
						me->SetCanFly(false);
						me->SetDisableGravity(false);
						SetCombatMovement(true);
						me->GetMotionMaster()->Clear(false);
						me->GetMotionMaster()->MoveChase(me->GetVictim());
						IsInFlight = false;
						canLand = false;
					}
					else
						uiLandTimer -= diff;
				}
			}
			if(!IsInFlight)
				DoMeleeAttackIfReady();
		}
	private:
		GameObject * groundTarget;
		Creature * trigger;
		SummonList summons;
	};

	CreatureAI * GetAI(Creature * pCreature) const OVERRIDE
	{
		return new npc_erithol_finalAI(pCreature);
	}
};

class remove_non_battle_player : public PlayerScript
{
public:
	remove_non_battle_player() : PlayerScript("remove_non_battle_player") { }

	void OnUpdateZone(Player * player, uint32 zone, uint32 area) OVERRIDE
	{
		if(m_PlayerGUID == 0)
			return;

		if(player->GetZoneId() != KARAZHAN_ZONE && player->GetAreaId() != KARAZHAN_AREA && player->GetGUID() == m_PlayerGUID)
		{
			inZone = false;
			return;
		}

		if(player->GetAreaId() != KARAZHAN_AREA || player->GetSession()->GetSecurity() > 1)
			return;

		if(isBattleActive && player->GetGUID() != m_PlayerGUID)
		{
			player->TeleportTo(player->GetStartPosition().GetMapId(), player->GetStartPosition().GetPositionX(), player->GetStartPosition().GetPositionY(),
				player->GetStartPosition().GetPositionZ(), player->GetStartPosition().GetOrientation());
			ChatHandler(player->GetSession()).SendSysMessage("You cannot be in the battle area while the event is going on!");
		}
	}

	void OnLogout(Player * player) OVERRIDE
	{
		if(m_PlayerGUID == 0)
			return;

		if(player->GetGUID() == m_PlayerGUID)
			hasLogged = true;
	}
};

void AddSC_erithol_battle()
{
	/* Npc Classes */
	new npc_event_arena_commander;
	new npc_erithol_event_demon_guard;
	new npc_demon_guard_mini;
	new npc_wave_trigger;
	new npc_wave_spawns;
	new npc_xiombarg_the_tank;
	new npc_field_medic;
	new npc_main_sorc;
	new npc_sorc_initiate;
	new npc_main_unholy;
	new npc_unholy_twin;
	new npc_unholy_pet;
	new npc_army_ghoul;
	new npc_shaman_guardian;
	new npc_shaman_brute;
	new npc_erithol_final;
	new npc_wave_portals;
	/* Player Classes */
	new remove_non_battle_player;
}