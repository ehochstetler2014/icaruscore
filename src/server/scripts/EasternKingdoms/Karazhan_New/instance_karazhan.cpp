/**************************************************\
*   Instance Karazhan: (C) Umbra Gaming Inc 2013  *
*     Written By Albag & Nobody @ Umbra Gaming    *
*                  UC4-> 255 Core                 *
/**************************************************\

/* ScriptData
SDName: Instance Karazhan
SDComment: Need more work.
SDData:5%
SDCategory: Karazhan
EndScriptData */

#include "ScriptPCH.h"
#include "Player.h"
#include "karazhan_inc.h"
#define MAX_ENCOUNTER  12

class instance_karazhan : public InstanceMapScript
{
public:
	instance_karazhan() : InstanceMapScript("instance_karazhan", 532) { } 

	InstanceScript* GetInstanceScript(InstanceMap* pMap) const
	{
		return new instance_karazhan_InstanceMapScript(pMap);
	}

	struct instance_karazhan_InstanceMapScript : public InstanceScript
	{
		instance_karazhan_InstanceMapScript(Map* pMap) : InstanceScript(pMap) {}

		uint32 m_auiEncounter[MAX_ENCOUNTER];
		uint32 TeamInInstance;

		uint16 uiMovementDone;
		uint16 uiGrandChampionsDeaths;
		uint8 uiArgentSoldierDeaths;
		uint8 uiAgroDone;
		uint8 uiAggroDone;
		std::string str_data;
		bool bDone;

		void Initialize() // Need to Find out more Data about How we should get it to work.
		{
			uiMovementDone = 0;
			bDone = false;
			memset(&m_auiEncounter, 0, sizeof(m_auiEncounter));
		}

		bool IsEncounterInProgress() const
		{
			for (uint8 i = 0; i < MAX_ENCOUNTER; ++i)
			{
				if (m_auiEncounter[i] == IN_PROGRESS)
					return true;
			}

			return false;
		}
		void HandleSpellOnPlayersInInstanceKarazhan(Unit* caller, uint32 spellId) // Lets check and gic right spell to players in the zone
		{
			if (spellId <= 0 || !caller)
				return;

			Map* map = caller->GetMap();
			if (map && map->IsDungeon())
			{
				Map::PlayerList const &PlayerList = map->GetPlayers();

				if (PlayerList.isEmpty())
					return;

				for (Map::PlayerList::const_iterator i = PlayerList.begin(); i != PlayerList.end(); ++i)
					if (i->GetSource() && i->GetSource()->IsAlive() && !i->GetSource()->IsGameMaster())
						caller->CastSpell(i->GetSource(), spellId);
			}
		}
	};
};

void AddSC_instance_karazhan()
{
	new instance_karazhan();
}