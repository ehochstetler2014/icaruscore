/********************************\
*    	  335_BotCore   		 *
*      Boss Yarovath Fight       *
*        Type: Header File       *
*     		By JCarter 			 *
\********************************/

#include "ScriptPCH.h"
using namespace std;

uint64 y_PlayerGUID;
string y_playerName;
bool y_isBattleActive = false;
bool y_hasLogged = false;
bool y_inZone = true;
int y_isWaveBossDead = 0;

#define MSG_FIGHT_COMPUTER "Disturb the wizard!"

enum eventIds
{
	EVENT_NONE,
	EVENT_CHECK_ACTIVITY,
	EVENT_CHECK_WAVES,
	EVENT_CHECK_PLAYER,
	EVENT_FIRST_WAVE,
	EVENT_SECOND_WAVE,
	EVENT_THIRD_WAVE,
	EVENT_FOURTH_WAVE,
	EVENT_COMPLETED_WAVES,
};

enum eEnums
{
	KARAZHAN_ZONE = 3457,
	KARAZHAN_AREA = 3457,
};

static Position sTeleOut[] =
{
	{ -11240.402f, -1817.602f, 223.943f, 5.398f } /* Get new tele location with quest giver */
};

enum Y_Phases
{
	/* Final Boss Phases */
	PHASE_START_COMBAT,
	PHASE_END,
};

enum SpawnIds
{
	/* Boss Waves */
	NPC_MINI_BOSS_ONE =				85005, // Mortuum
	NPC_MINI_BOSS_TWO =				85004, // Lues
	NPC_MINI_BOSS_THREE =			85003, // Glacia
	NPC_MINI_BOSS_FOUR =			85002, // Fieros
	NPC_MINI_BOSS_PORTAL =			85001,
	/* Event Boss */
	BOSS_YAROVATH = 				85000, /* (Neutral Boss for Event Trigger) */
};
enum SpellIds
{
	/* Mini Boss One Spells */
	SPELL_BONE_SLICE = 				69055, // Splits 200% of normal melee damage to an enemy and its two nearest allies.
	SPELL_BONE_SHIELD = 			27688, // Absorbs damage. 100 damage inflicted to melee and ranged attackers. 100 damage inflicted to spell casters. (5 Min)
	SPELL_ANTIMAGIC_SHELL = 		48707, // Spell damage reduced by 75%. Immune to magic debuffs.
	SPELL_SHADOW_STORM =			2148,  // Periodically casts Shadow Storm.
	SPELL_DEATH_AND_DECAY = 		72110, // 6000 Shadow damage inflicted every 1 sec to all targets in the affected area for 10 sec.
	SPELL_DEATHCOIL =				62904, // Fire a blast of unholy energy, causing 443 Shadow damage to an enemy target or healing 443 damage from a friendly Undead target.
	
	/* Mini Boss Two Spells */
	SPELL_DEATH_BLOOM = 			55594, // Inflicts 200 Nature damage every 1 sec. for 6 sec, followed by an additional 1200 Nature damage.
	SPELL_BLACK_PLAGUE = 			64155, // Retch uncontrollably for 2 sec.
	SPELL_BLOOD_PLAGUE = 			55322, // A vicious strike that deals weapon damage and plagues the target, dealing 30000 Shadow damage over 12 sec.
	SPELL_BROOD_PLAGUE = 			59467, // Diseases the enemy, causing 25500 Nature damage over 30 sec.

	/* Mini Boss Three Spells */
	SPELL_INCREASE_FROST_DAMAGE	=	17911, // Increases frost spell power by 54.
	SPELL_FROST_BREATH =			50505, // Inflicts 438 - 562 Frost damage to enemies in a 20 yards cone in front of the caster. In addition, the targets' attack speed is decreased by 100% for 6 sec.
	/* DoCast this on Lowest Aggro */
	SPELL_CHAINS_OF_ICE = 			22745, // Forms icy chains around the enemy, locking the target in place and burning 350 - 450 mana per 5 sec. for 10 sec.
	/* Random Target all 3 spells in order */
	SPELL_ICE_BOLT = 				28522, // Encases the target in a solid block of ice, stunning the target and dealing 7125 - 7875 Frost damage to the target and nearby enemies.
	SPELL_ICE_LANCE = 				46194, // Deals 657 - 843 Frost damage to an enemy target. Causes triple damage against Frozen targets.
	SPELL_ICE_BLAST = 				41579, // Inflicts Frost damage to an enemy. Shatters a target that is frozen, dealing double damage and shattering the binding ice.

	/* Mini Boss Four Spells */
	SPELL_MASSIVE_FIREBALL = 		26558, // Massive Fireball Visual Effect.
	SPELL_CHARRED_EARTH =			30129, // AOE Visual Effect
	SPELL_MOLTEN_SPEW = 			67637, // Deals 4625 - 5375 Fire damage per 0.25 sec. to enemies in front of the caster.
	SPELL_RAIN_OF_FIRE = 			58936, // Calls down a molten rain, burning all enemies in a selected area for 3700 - 4300 Fire damage every 2 sec. for 6 sec.
	/* DoCast these on Random Target */
	SPELL_FLAME_SHOCK =				49233, // For LAVA_BURST critical hit.
	SPELL_LAVA_BURST =				59182, // You hurl molten lava at the target, dealing 3956 - 5044 Fire damage. If Flame Shock is on the target, Lava Burst will consume the Flame Shock, causing Lava Burst to critically hit.
	SPELL_DRAGON_BREATH =			29964, //Sets an enemy aflame, inflicting 6000 Fire damage over 6 sec. and sending it into a state of panic. While the target is affected, the flames periodically scorch its nearby allies for 1000 damage as well.
 	
	/* Enrage */
	SPELL_FRENZY =		            48017, // Increases the caster's Physical damage by 100%.
	SPELL_FURY_OF_FROSTMOUNE =		70063, // Death, Deals 1000000 Shadow damage to all enemies.
	SPELL_MASTER_BUFF_MELEE	 =		35874, // Master Buff (Physical)
	SPELL_MASTER_BUFF_MAGIC	 =		35912, // Master Buff (Magical)
	SPELL_MASTER_BUFF_RANGE	 =		38734, // Master Buff (Ranged)
	SPELL_ASHBRINGER_FURY =	 		28754, // Strength increased by 300.
	SPELL_FRENZYHEART_FURY =		59821, // Your critical strike rating is increased by 91. (10 SECONDS)
};

enum Yells
{
	/* Mini Boss One */
	SAY_AGGRO_1                                     = 0, // Say on Battle Start
	SAY_PATHETIC_1                                  = 1, // Say on Player Death
	SAY_NOT_OVER_1                                  = 2, // Say on Boss Death
	/* Mini Boss Two */
	SAY_AGGRO_2                                     = 0, // Say on Battle Start
	SAY_PATHETIC_2                                  = 1, // Say on Player Death
	SAY_NOT_OVER_2                                  = 2, // Say on Boss Death
	/* Mini Boss Three */
	SAY_AGGRO_3                                     = 0, // Say on Battle Start
	SAY_PATHETIC_3                                  = 1, // Say on Player Death
	SAY_NOT_OVER_3                                  = 2, // Say on Boss Death
	/* Mini Boss Four */
	SAY_AGGRO_4                                     = 0, // Say on Battle Start
	SAY_PATHETIC_4                                  = 1, // Say on Player Death
	SAY_NOT_OVER_4                                  = 2, // Say on Boss Death
	SAY_TALK_RUNAWAY								= 3, // Charred Earth
};

#define MAX_WAVE_SPAWN_LOCATIONS 8
const uint32 y_waveList[MAX_WAVE_SPAWN_LOCATIONS] =
{
	/* First Wave Ids */
	NPC_MINI_BOSS_PORTAL, NPC_MINI_BOSS_ONE, // y_waveList[0]
	/* Second Wave Ids */
	NPC_MINI_BOSS_PORTAL, NPC_MINI_BOSS_TWO, // y_waveList[1]
	/* Third Wave Ids */
	NPC_MINI_BOSS_PORTAL, NPC_MINI_BOSS_THREE, // y_waveList[2]
	/* Fourth Wave Ids */
	NPC_MINI_BOSS_PORTAL, NPC_MINI_BOSS_FOUR,// y_waveList[3]
};

static Position y_WaveSpawns[] =
{
	/*    X               Y            Z           O      */
	/* Spawn Location */
	{ -11333.273f, -1782.604f, 179.734f, 3.570f }, // Portal Location y_WaveSpawns[0]
};

void yMessageOnWave(Creature * me, uint32 eventId)
{
	stringstream ss;
	switch(eventId)
	{
	case EVENT_CHECK_ACTIVITY: // Before Wave 1 starts
		ss << " Come forth great Demon and rid me of this nuisance";
		break;

	case EVENT_CHECK_WAVES:
		{
			if( y_isWaveBossDead == 1)
			{
				ss << "What, Mortuus dead..." 
					<< " Very well, a more powerful demon then!!!";
			}
			else if (y_isWaveBossDead == 2)
			{
				ss << y_playerName.c_str()
					<< "You have taken my precious Lues!" 
					<< " You and your comrades will pay with your very souls!!!"
					<< " Come forth Glacies!!!";
			}
			else if (y_isWaveBossDead == 3)
			{
				ss << "This cannot be!!!" 
					<< "No Time... no time..." 
					<< "Fieros take them to the fiery pits of hell!!!";
			}
		}break;

	case EVENT_FIRST_WAVE:
		ss << "The summoning ritual has ended!"
			<< " A portal appears and something sinister steps out from the depths of Hell!";
		break;

	case EVENT_SECOND_WAVE:
		ss << "The summoning ritual has ended!"
			<< " A new threat has emerged from the dark depths of the nether void!";
		break;

	case EVENT_THIRD_WAVE:
		ss << "The summoning ritual has ended!"
			<< " Quickly dispatch the threat heroes!";
		break;

	case EVENT_FOURTH_WAVE:
		ss << "The summoning ritual has ended!"
			<< " Only pain can come from this battle!"
			<< " Be Prepared!";
		break;
	}
	me->MonsterYell(ss.str().c_str(), LANG_UNIVERSAL, me);
}

void yDoEndBattle(Creature * me)
{
	y_isBattleActive = false;
	y_PlayerGUID = NULL;
	y_playerName = "";
	me->DespawnOrUnsummon(1);
}

void yDoEndBattle(Creature * me, SummonList summons)
{
	y_isBattleActive = false;
	y_PlayerGUID = NULL;
	y_playerName = "";
	summons.DespawnAll();
	me->DespawnOrUnsummon(1);
}	   